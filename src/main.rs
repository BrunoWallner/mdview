mod events;
mod parser;
mod render;
mod root;
mod widgets;

use std::io::Read;
use std::time::Duration;

use std::env;
use std::fs;

const TICK_RATE: u64 = 60;

fn main() {
    let terminal = render::Terminal::init();
    let event_handler = events::EventHandler::new(Duration::from_millis(1000 / TICK_RATE));

    let mut root = root::Root::new(terminal, event_handler);
    // root.add_widget(Header::new(
    //     1, "Die Deutsche Autoindustrie"
    // ));
    // root.add_widget(Header::new(
    //     2, "S: 71/4"
    // ));
    // root.add_widget(Text::new()
    //     .with_type(TextType::Normal("Die Deutsche Autoindustrie hat bereits "))
    //     .with_type(TextType::Bold("164"))
    //     .with_type(TextType::Normal(" Autos hergestellt"))
    // );

    // root.add_widget(Text::new()
    //     .with_type(TextType::Normal("Im Interview mit Ralf Zahn:"))
    // );
    // root.add_widget(BlockQuote::new()
    //     .with_widget(Text::new()
    //         .with_type(TextType::Normal("wir haben schon über 100 Autos hergestellt\n"))
    //         .with_type(TextType::Normal("unsere Autos sind definitiv eines der Autos aller Zeiten"))
    //     )
    // );
    if let Some(markdown_path) = env::args().nth(1) {
        if let Ok(mut file) = fs::File::open(markdown_path) {
            let mut markdown = String::new();
            file.read_to_string(&mut markdown).unwrap();

            root.div = parser::parse(&markdown);

            root.init();
        } else {
            drop(root);
            eprintln!("could not read file");
        }
    } else {
        drop(root);
        eprintln!("invalid path");
    }
}
