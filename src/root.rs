use crate::render::{PixelGrid, Terminal};
use crate::widgets::{Context, Div, WidgetTrait};

use crate::events::{Event, EventHandler};
use termion::event::{Key, MouseButton, MouseEvent};

const MIN_WIDTH: usize = 40;
const SCROLL_SPEED: isize = 5;

pub struct Root {
    terminal: Terminal,
    event_handler: EventHandler,
    pixel_grid: PixelGrid,
    pub div: Div,
}
impl Root {
    pub fn new(terminal: Terminal, event_handler: EventHandler) -> Self {
        let term_size = terminal.size;
        let width = if (term_size.0 as usize) < MIN_WIDTH {
            MIN_WIDTH
        } else {
            term_size.0 as usize
        };
        Self {
            terminal,
            event_handler,
            pixel_grid: PixelGrid::new((width, term_size.1 as usize)),
            div: Div::new(),
        }
    }

    pub fn render_widgets(&mut self) {
        self.pixel_grid.clear();

        // draws every widget
        let mut ctx = Context {
            screen_size: (self.terminal.size.0 as usize, self.terminal.size.1 as usize),
            current_pos: (0, 0),
            indentation: 0,
        };

        self.pixel_grid = self.div.draw(&mut ctx).0;
    }

    pub fn resize(&mut self, size: (u16, u16)) {
        self.pixel_grid.resize_x(size.0 as usize);
        self.pixel_grid.resize_y(size.1 as usize);
    }

    pub fn init(&mut self) {
        self.render_widgets();
        let mut offset = (0, 0);

        loop {
            // preparation for rendering
            self.terminal.clear();
            self.render_widgets();
            // offset, TODO! implement main widget to do all of this
            let mut offset_grid = self.pixel_grid.clone(); // forgive me
            offset_grid.apply_offset(offset);

            // rendering
            self.terminal.draw(&offset_grid);

            // event handling
            match self.event_handler.get().unwrap() {
                Event::Input(input) => match input {
                    Key::Char('q') => break,

                    // moving
                    Key::Left => offset.0 -= 3,
                    Key::Right => offset.0 += 3,
                    Key::Up => offset.1 -= 3,
                    Key::Down => offset.1 += 3,
                    Key::Char('c') => offset.0 = 0,
                    Key::Char('b') => offset.1 = 0,
                    _ => (),
                },
                Event::Mouse(event) => match event {
                    MouseEvent::Press(btn, _x, _y) => match btn {
                        MouseButton::WheelDown => offset.1 += SCROLL_SPEED,
                        MouseButton::WheelUp => offset.1 -= SCROLL_SPEED,
                        _ => (),
                    },
                    _ => (),
                },
                Event::Resize(size) => {
                    self.resize(size);
                }
            }
        }
    }
}
