use super::{Context, Position, Widget, Br, WidgetTrait};
use crate::render::PixelGrid;

const PADDING: usize = 1;

// TODO: calculate width and height when pushing widgets to save perf
#[derive(Debug, Clone, PartialEq)]
pub struct Div {
    inner: Vec<Widget>,
}
impl Div {
    pub fn new() -> Self {
        Self { inner: Vec::new() }
    }
}
impl WidgetTrait for Div {
    fn draw(&self, ctx: &mut Context) -> (PixelGrid, Position) {
        let mut grid = PixelGrid::new((0, 0));

        let mut inner_ctx = Context {
            screen_size: ctx.screen_size,
            current_pos: (0, 0),
            indentation: 0,
        };

        let (mut width, mut _height) = (0, 0);
        let mut widgets = self.inner.iter().peekable();
        while let Some(wid) = widgets.next() {
            let (rend, pos_next) = wid.draw(&mut inner_ctx);
            let (w, _h) = rend.size;

            grid.merge(&rend, inner_ctx.current_pos);

            inner_ctx.current_pos = pos_next;

            width = std::cmp::max(width, w);

            // additional padding if widgets differ
            if let Some(next) = widgets.peek() {
                let br = &Widget::Br(Br::new());
                if *next != wid && *next != br && wid != br {
                    inner_ctx.current_pos.1 += PADDING;
                }
            }
        }


        let position_next = (ctx.indentation, ctx.current_pos.1 + grid.size.1);

        (grid, position_next)
    }

    fn add_widget(&mut self, widget: Widget) {
        self.inner.push(widget);
    }
}
