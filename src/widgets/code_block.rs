use super::{Context, Position, Div, Widget, WidgetTrait};
use crate::render::PixelGrid;

// TODO: calculate width and height when pushing widgets to save perf
#[derive(Debug, Clone, PartialEq)]
pub struct CodeBlock {
    inner: Div,
}
impl CodeBlock {
    pub fn new() -> Self {
        Self { inner: Div::new() }
    }
}
impl WidgetTrait for CodeBlock {
    fn draw(&self, ctx: &mut Context) -> (PixelGrid, Position) {
        let mut grid = PixelGrid::new((0, 0));

        // creates and renders inner div
        let mut inner_ctx = Context {
            screen_size: ctx.screen_size,
            current_pos: (1, 0), // x1 because of left bar
            indentation: 0,
        };

        // let mut inner_div = Div::new();

        // let (mut width, mut height) = (0, 0);
        // for wid in self.inner.iter() {
        //     let (rend, pos_next) = wid.draw(&mut inner_ctx);
        //     let (w, h) = rend.size;

        //     grid.merge(&rend, inner_ctx.current_pos);

        //     inner_ctx.current_pos = pos_next;

        //     width = std::cmp::max(width, w);
        //     height += h;
        // }


        // render inner, but merg later, in order to compensate for variable line count width
        let (inner_grid, _pos_next) = self.inner.draw(&mut inner_ctx);
        let (width, height) = inner_grid.size;

        // line count
        let max_num = height.to_string().len();
        for y in 0..height {
            for (x, char) in y.to_string().chars().enumerate() {
                grid.set((x, y), Some(char), Some([50, 100, 150]), None);
            }
        }

        // left bar
        for y in 0..height {
            grid.set((max_num, y), Some('▌'), Some([50, 100, 150]), None);
        }

        // merge inner to outer
        grid.merge(&inner_grid, (max_num + 1, 0));

        // background
        for x in 0..width + 1 + max_num {
            for y in 0..height {
                grid.set((x, y), None, None, Some([40, 40, 60]));
            }
        }

        let position_next = (ctx.indentation, ctx.current_pos.1 + grid.size.1);

        (grid, position_next)
    }

    fn add_widget(&mut self, widget: Widget) {
        self.inner.add_widget(widget);
    }
}
