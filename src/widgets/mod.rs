pub mod code_block;
pub mod block_quote;
pub mod br;
pub mod div;
pub mod header;
pub mod text;

use crate::render::PixelGrid;

pub use code_block::CodeBlock;
pub use block_quote::BlockQuote;
pub use br::Br;
pub use div::Div;
pub use header::Header;
pub use text::Text;

pub type Position = (usize, usize);

pub const LINE_WRAP: usize = 80;

#[derive(Clone)]
pub struct Context {
    pub screen_size: Position, // basically the same
    pub current_pos: Position,
    pub indentation: usize,
}

pub trait WidgetTrait {
    fn draw(&self, ctx: &mut Context) -> (PixelGrid, Position);
    fn add_widget(&mut self, widget: Widget);
}

#[derive(Debug, Clone, PartialEq)]
pub enum Widget {
    CodeBlock(CodeBlock),
    BlockQuote(BlockQuote),
    Br(Br),
    Header(Header),
    Div(Div),
    Text(Text),
}
impl WidgetTrait for Widget {
    fn draw(&self, ctx: &mut Context) -> (PixelGrid, Position) {
        match self {
            Self::CodeBlock(w) => w.draw(ctx),
            Self::BlockQuote(w) => w.draw(ctx),
            Self::Br(w) => w.draw(ctx),
            Self::Header(w) => w.draw(ctx),
            Self::Div(w) => w.draw(ctx),
            Self::Text(w) => w.draw(ctx),
        }
    }

    fn add_widget(&mut self, widget: Widget) {
        match self {
            Self::CodeBlock(w) => w.add_widget(widget),
            Self::BlockQuote(w) => w.add_widget(widget),
            Self::Br(w) => w.add_widget(widget),
            Self::Header(w) => w.add_widget(widget),
            Self::Div(w) => w.add_widget(widget),
            Self::Text(w) => w.add_widget(widget),
        }
    }
}
