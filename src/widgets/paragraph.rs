// use super::{Context, Position, Text, Widget, WidgetTrait, LINE_WRAP};
// use crate::render::PixelGrid;

// #[derive(Clone)]
// pub struct Paragraph {
//     inner: Vec<Text>,
// }
// impl Paragraph {
//     pub fn new() -> Self {
//         Self { inner: Vec::new() }
//     }
// }
// impl WidgetTrait for Paragraph {
//     fn draw(&self, ctx: &mut Context) -> (PixelGrid, Position) {
//         let mut grid = PixelGrid::new((0, 0));

//         let mut ctx = Context {
//             screen_size: (0, 0),
//             current_pos: (0, 0),
//             indentation: 0,
//         };

//         for text_type in self.inner.iter() {
//             let (inner_grid, pos_next) = text_type.draw(&mut ctx);
//             grid.merge(&inner_grid, ctx.current_pos);
//             ctx.current_pos = pos_next;
//         }

//         (grid, ctx.current_pos)
//     }

//     fn add_widget(&mut self, widget: Widget) {
//         match widget {
//             Widget::Text(wid) => self.inner.push(wid),
//             _ => (),
//         }
//     }
// }
