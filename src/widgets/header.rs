use super::{Context, Div, Position, Widget, WidgetTrait};
use crate::render::PixelGrid;

const INDENTATION_WIDTH: usize = 2;

#[derive(Debug, Clone, PartialEq)]
pub struct Header {
    pub level: u8,
    pub inner: Div,
}
impl Header {
    pub fn new(level: u8) -> Self {
        Self {
            level,
            inner: Div::new(),
        }
    }
}
impl WidgetTrait for Header {
    fn draw(&self, ctx: &mut Context) -> (PixelGrid, Position) {
        let mut grid = PixelGrid::new((0, 0));

        let pixel_grid = self.inner.draw(ctx).0;
        grid.merge(&pixel_grid, (0, 1)); // 1 upper padding

        // underline
        let line: Option<char> = match self.level {
            1 => Some('▔'),
            2 => Some('‾'),
            3 => Some('¯'),
            _ => None,
        };
        if let Some(line) = line {
            for x in 0..pixel_grid.size.0 {
                grid.set((x, 2), Some(line), None, None);
            }
        };

        // bg color
        let color = match self.level {
            1 => Some(([255, 255, 255], [110, 10, 40])),
            2 => Some(([255, 255, 255], [122, 13, 122])),
            3 => Some(([255, 255, 255], [14, 99, 62])),
            4 => Some(([255, 255, 255], [17, 62, 101])),
            5 => Some(([255, 255, 255], [53, 26, 98])),
            _ => None,
        };
        if let Some((fg, bg)) = color {
            for x in 0..pixel_grid.size.0 {
                grid.set((x, 1), None, Some(fg), Some(bg));
            }
        }

        // position
        ctx.current_pos = match self.level {
            1 => {
                if ctx.screen_size.0 > grid.size.0 {
                    (ctx.screen_size.0 / 2 - grid.size.0 / 2, ctx.current_pos.1)
                } else {
                    ctx.current_pos
                }
            }
            _ => ctx.current_pos,
        };

        // indentation
        ctx.indentation = self.level as usize * INDENTATION_WIDTH;

        let position_next = (
            ctx.indentation,
            ctx.current_pos.1 + 3, /* <-- always keep 2 rows space, 1 upper padding */
        );

        (grid, position_next)
    }

    fn add_widget(&mut self, widget: Widget) {
        self.inner.add_widget(widget);
    }
}
