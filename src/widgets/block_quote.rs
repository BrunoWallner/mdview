use super::{Context, Position, Widget, WidgetTrait};
use crate::render::PixelGrid;

// TODO: calculate width and height when pushing widgets to save perf
#[derive(Debug, Clone, PartialEq)]
pub struct BlockQuote {
    inner: Vec<Widget>,
}
impl BlockQuote {
    pub fn new() -> Self {
        Self { inner: Vec::new() }
    }
}
impl WidgetTrait for BlockQuote {
    fn draw(&self, ctx: &mut Context) -> (PixelGrid, Position) {
        let mut grid = PixelGrid::new((0, 0));

        let mut inner_ctx = Context {
            screen_size: ctx.screen_size,
            current_pos: (1, 0), // x1 because of left bar
            indentation: 0,
        };

        let (mut width, mut height) = (0, 0);
        for wid in self.inner.iter() {
            let (rend, pos_next) = wid.draw(&mut inner_ctx);
            let (w, h) = rend.size;

            grid.merge(&rend, inner_ctx.current_pos);

            inner_ctx.current_pos = pos_next;

            width = std::cmp::max(width, w);
            height += h;
        }

        // background
        for x in 0..width + 1 {
            for y in 0..height {
                grid.set((x, y), None, None, Some([40, 40, 60]));
            }
        }

        // left bar
        for y in 0..height {
            grid.set((0, y), Some('▌'), Some([50, 100, 150]), None);
        }

        let position_next = (ctx.indentation, ctx.current_pos.1 + grid.size.1);

        (grid, position_next)
    }

    fn add_widget(&mut self, widget: Widget) {
        self.inner.push(widget);
    }
}
