use super::{Context, Position, WidgetTrait, LINE_WRAP};
use crate::render::PixelGrid;

#[allow(dead_code)]
#[derive(Debug, Clone, PartialEq)]
pub enum Text {
    Normal(String),
    Bold(String),
    Italic(String),
    BoldItalic(String),
    InlineCode(String),
}
impl Text {
    pub fn get_text(&self) -> String {
        match self {
            Text::Normal(t) => t.clone(),
            Text::Bold(t) => t.clone(),
            Text::Italic(t) => t.clone(),
            Text::BoldItalic(t) => t.clone(),
            Text::InlineCode(t) => t.clone(),
        }
    }
}

impl WidgetTrait for Text {
    fn draw(&self, ctx: &mut Context) -> (PixelGrid, Position) {
        let mut grid = PixelGrid::new((0, 0));

        let mut outer_x: usize = 0;
        let mut outer_y: usize = 0;

        let (text, fg, bg) = match &self {
            Text::Normal(text) => (text, None, None),
            Text::Bold(text) => (text, Some([255, 255, 255]), Some([20, 20, 20])),
            Text::Italic(text) => (text, Some([100, 100, 255]), Some([20, 20, 20])),
            Text::BoldItalic(text) => (text, Some([255, 100, 100]), Some([20, 20, 20])),
            Text::InlineCode(text) => (text, Some([255, 255, 255]), Some([12, 54, 51])),
        };

        // for char in text.chars() {
        //     // primitive textwrap
        //     grid.set((outer_x, outer_y), Some(char), fg, bg);
        //     outer_x += 1;
        //     if outer_x % LINE_WRAP == 0 || char == '\n' || char == '\\' { // might want to check if x > 0
        //         outer_x = 0;
        //         outer_y += 1;
        //     } 
        // }

        for char in text.chars() {
            match char {
                '\n' => {
                    outer_x = 0;
                    outer_y += 1;
                },
                char => {
                    grid.set((outer_x, outer_y), Some(char), fg, bg);
                    outer_x += 1;
                }
            }
        }

        let position_next = (ctx.current_pos.0 + outer_x, ctx.current_pos.1 + outer_y);

        (grid, position_next)
    }

    fn add_widget(&mut self, _widget: super::Widget) {}
}
