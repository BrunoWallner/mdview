use super::{Context, Position, Widget, WidgetTrait};
use crate::render::PixelGrid;

#[derive(Debug, Clone, PartialEq)]
pub struct Br;

impl Br {
    pub fn new() -> Self {
        Br {}
    }
}

impl WidgetTrait for Br {
    fn draw(&self, ctx: &mut Context) -> (PixelGrid, Position) {
        (
            PixelGrid::new((0, 0)),
            (ctx.indentation, ctx.current_pos.1 + 1),
        )
    }
    fn add_widget(&mut self, _widget: Widget) {
        debug_assert!(false); // could be triggered with invalid Markdown
    }
}
