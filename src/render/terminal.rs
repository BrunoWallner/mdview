use super::PixelGrid;
use std::io::{stdout, Stdout, Write};
use termion::input::MouseTerminal;
use termion::raw::{IntoRawMode, RawTerminal};
use termion::screen::AlternateScreen;

pub struct Terminal {
    screen: MouseTerminal<AlternateScreen<RawTerminal<Stdout>>>,
    pub size: (u16, u16),
}

impl Terminal {
    pub fn init() -> Self {
        let raw = stdout().into_raw_mode().unwrap();
        let screen = AlternateScreen::from(raw);
        let mut screen = termion::input::MouseTerminal::from(screen);
        write!(screen, "{}", termion::cursor::Hide).unwrap();
        write!(screen, "{}", termion::clear::All).unwrap();

        let size = termion::terminal_size().unwrap();

        Self { screen: screen, size}
    }
    pub fn draw(&mut self, grid: &PixelGrid) {
        let size = self.size();
        let lines = get_lines(grid, size);

        let mut lock = self.screen.lock();
        {
            write!(lock, "{}", termion::cursor::Goto(0, 1)).unwrap();
            for (y, line) in lines.iter().enumerate() {
                write!(self.screen, "{}", termion::cursor::Goto(0, y as u16 + 1)).unwrap();
                write!(lock, "{}", line).unwrap();
            }
        } // dropping lock

        self.screen.flush().unwrap();
    }
    pub fn clear(&mut self) {
        write!(self.screen, "{}", termion::clear::All).unwrap();
    }
    pub fn size(&mut self) -> (u16, u16) {
        self.size = termion::terminal_size().unwrap();
        self.size
    }
}

use termion::color;

// converts 2D array to 1D line array
// also applies color and formatting
pub fn get_lines(grid: &PixelGrid, size: (u16, u16)) -> Vec<String> {
    //let empty_line: String = std::iter::repeat(" ").take(size.0 as usize).collect::<String>();
    let mut lines: Vec<String> = vec![String::new(); size.1 as usize];

    for y in 0..size.1 as usize {
        for x in 0..size.0 as usize {
            if let Some(line) = grid.grid.get(y) {
                if let Some(pixel) = line.get(x) {
                    // foreground
                    if let Some(fg_color) = pixel.fg_color {
                        lines[y].push_str(&format!(
                            "{}",
                            color::Fg(color::Rgb(fg_color[0], fg_color[1], fg_color[2]))
                        ));
                    } else {
                        lines[y].push_str(&format!("{}", color::Fg(color::Reset)));
                    }

                    // background
                    if let Some(bg_color) = pixel.bg_color {
                        lines[y].push_str(&format!(
                            "{}",
                            color::Bg(color::Rgb(bg_color[0], bg_color[1], bg_color[2]))
                        ));
                    } else {
                        lines[y].push_str(&format!("{}", color::Bg(color::Reset)));
                    }
                    // char
                    if let Some(character) = pixel.character {
                        lines[y].push_str(&character.to_string());
                    } else {
                        lines[y].push_str(" ");
                    }
                }
            }
        }
        // visual bugs if not done
        lines[y].push_str(&format!(
            "{}",
            color::Bg(color::Reset)
        ));
    }

    lines
}
