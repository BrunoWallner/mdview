pub mod terminal;

pub use terminal::Terminal;

#[derive(Copy, Clone)]
pub struct Pixel {
    pub character: Option<char>,
    pub fg_color: Option<[u8; 3]>,
    pub bg_color: Option<[u8; 3]>,
}
impl Pixel {
    pub fn new() -> Self {
        Pixel {
            character: None,
            fg_color: None,
            bg_color: None,
        }
    }
}

#[derive(Clone)]
pub struct PixelGrid {
    pub grid: Vec<Vec<Pixel>>,
    pub size: (usize, usize),
}
impl PixelGrid {
    pub fn new(size: (usize, usize)) -> Self {
        Self {
            grid: vec![vec![Pixel::new(); size.0]; size.1],
            size,
        }
    }

    pub fn set(
        &mut self,
        pos: (usize, usize),
        c: Option<char>,
        fg: Option<[u8; 3]>,
        bg: Option<[u8; 3]>,
    ) {
        if self.size.0 <= pos.0 {
            self.resize_x(pos.0 + 1)
        }
        if self.size.1 <= pos.1 {
            self.resize_y(pos.1 + 1)
        }
        let (x, y) = pos;

        if c.is_some() {
            self.grid[y][x].character = c
        }
        if fg.is_some() {
            self.grid[y][x].fg_color = fg
        }
        if bg.is_some() {
            self.grid[y][x].bg_color = bg
        }
    }

    pub fn merge(&mut self, other: &Self, pos: (usize, usize)) {
        for x in 0..other.size.0 {
            for y in 0..other.size.1 {
                self.set(
                    (x + pos.0, y + pos.1),
                    other.grid[y][x].character,
                    other.grid[y][x].fg_color,
                    other.grid[y][x].bg_color,
                )
            }
        }
    }

    pub fn resize_x(&mut self, size: usize) {
        if self.size.0 < size {
            let diff: usize = size - self.size.0;
            for line in self.grid.iter_mut() {
                line.append(&mut vec![Pixel::new(); diff]);
            }
        } else if self.size.0 > size {
            let diff: usize = self.size.0 - size;
            for line in self.grid.iter_mut() {
                for _ in 0..diff {
                    line.pop();
                }
            }
        }

        self.size.0 = size;
    }

    pub fn resize_y(&mut self, size: usize) {
        if self.size.1 < size {
            let diff: usize = size - self.size.1;
            self.grid
                .append(&mut vec![vec![Pixel::new(); self.size.0]; diff])
        }
        if self.size.1 > size {
            let diff: usize = self.size.1 - size;
            self.grid.drain((self.grid.len() - 1 - diff)..);
        }

        self.size.1 = size;
    }

    pub fn clear(&mut self) {
        self.grid = vec![vec![Pixel::new(); self.size.0]; self.size.1];
    }

    pub fn apply_offset(&mut self, offset: (isize, isize)) {
        // y
        if offset.1 > 0 {
            if self.grid.len() > offset.1 as usize {
                self.grid.drain(0..offset.1 as usize);
            } else {
                self.grid.drain(..);
            }
        } else if offset.1 < 0 {
            let off = (offset.1 as f32).abs() as usize;

            let mut empty_pixel = Pixel::new();
            empty_pixel.character = Some(' ');

            for _ in 0..off {
                self.grid.insert(0, vec![empty_pixel; self.size.0]);
            }
        }

        // x
        if offset.0 > 0 {
            for line in self.grid.iter_mut() {
                if line.len() > offset.0 as usize {
                    line.drain(0..offset.0 as usize);
                } else {
                    line.drain(..);
                }
            }
        } else if offset.0 < 0 {
            let off = (offset.0 as f32).abs() as usize;

            let mut empty_pixel = Pixel::new();
            empty_pixel.character = Some(' ');

            for line in self.grid.iter_mut() {
                for _ in 0..off {
                    line.insert(0, empty_pixel);
                }
            }
        }
    }
}
