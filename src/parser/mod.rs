use crate::widgets::*;
use pulldown_cmark::{Event, Options, Parser, Tag};

pub fn parse(input: &str) -> Div {
    let options = Options::empty();
    let parser = Parser::new_ext(input, options);

    let mut div: Div = Div::new();

    let mut widgets: Vec<Widget> = Vec::new();
    let mut index: isize = -1; // future cast into usize, so Event::Start MUST COME BEFORE Event::End
    parser.for_each(|event| {
        match event {
            Event::Start(tag) => {
                match tag {
                    Tag::Paragraph => widgets.push(Widget::Div(Div::new())),
                    Tag::Heading(level, _, _) => widgets.push(Widget::Header(Header::new(level as u8))),
                    Tag::BlockQuote => widgets.push(Widget::BlockQuote(BlockQuote::new())),
                    Tag::CodeBlock(_kind) => widgets.push(Widget::CodeBlock(CodeBlock::new())),
                    tag => {
                        let error = Widget::Text(Text::BoldItalic(format!("unimplemented tag: {:?}", tag)));
                        let mut div = Widget::Div(Div::new());
                        div.add_widget(error);

                        widgets.push(div);
                    },
                }
                index += 1;
            },
            Event::End(_tag) => {
                if index == 0 {
                    div.add_widget(widgets.remove(0));
                    index = -1;
                } else {
                    let wid = widgets.pop().unwrap();
                    index -= 1;
                    widgets[index as usize].add_widget(wid);
                }
            }
            Event::Text(text) => {
                let text = Widget::Text(Text::Normal(text.to_string()));
                widgets[index as usize].add_widget(text);
            }
            Event::Code(text) => {
                let text = Widget::Text(Text::InlineCode(text.to_string()));
                widgets[index as usize].add_widget(text);
            }
            Event::SoftBreak | Event::HardBreak => {
                widgets[index as usize].add_widget(Widget::Br(Br::new()));
            }
            _ => (),
        }
    }
    );

    // debug
    // let mut deb = Widget::CodeBlock(CodeBlock::new());
    // let txt = Widget::Text(Text::InlineCode(format!("{:#?}", div)));
    // deb.add_widget(txt);

    // div.add_widget(deb);

    div
}
